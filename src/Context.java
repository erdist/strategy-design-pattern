
public class Context {
	private Strategy strategy;
	
	public Context(Strategy strategy){
		this.strategy = strategy;
	}
	
	public String executeStrategy(String str1, String str2){
		return strategy.doOperation(str1, str2);
	}
}
