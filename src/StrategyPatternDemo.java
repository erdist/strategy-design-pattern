import java.util.Scanner;

public class StrategyPatternDemo {
	public static void main(String args[]){
		Context context;
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String str1,str2;
		
		while(!s.equalsIgnoreCase("Stop")){
			if(s.equalsIgnoreCase("Concatenate")){
				str1 = in.nextLine();
				str2 = in.nextLine();
				context = new Context(new OperationConcatenate());
				System.out.println(context.executeStrategy(str1, str2));
			}
			
			else if(s.equalsIgnoreCase("Equal")){
				str1 = in.nextLine();
				str2 = in.nextLine();
				context = new Context(new OperationEqual());
				System.out.println(context.executeStrategy(str1, str2));
			}
			
			else if(s.equalsIgnoreCase("Length")){
				str1 = in.nextLine();
				str2 = in.nextLine();
				context = new Context(new OperationLength());
				System.out.println(context.executeStrategy(str1, str2));
			}
			
			else{
				System.out.println("Wrong Command! Available commands are: 'Concatenate', 'Equal', 'Length' and 'Stop'");
			}
			
			s = in.nextLine();
			
		}
		in.close();
		
	}
}
