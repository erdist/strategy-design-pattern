
public interface Strategy {
	public String doOperation(String str1, String str2);
	
}
